package br.una.conferenza.cidade;

import javax.persistence.*;

import br.una.conferenza.estado.EstadoEntity;
import br.una.conferenza.utils.BaseEntity;

@Entity
@Table(name = "tb_cidade")
@AttributeOverride(name = "id", column = @Column(name = "codigo_cidade"))
public class CidadeEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome_cidade", length = 50, nullable = false)
    private String nomeCidade;

    @ManyToOne
    @JoinColumn(name = "codigo_uf", nullable = false)
    private EstadoEntity estado;

    public CidadeEntity() {

    }

    public CidadeEntity(String nomeCidade, EstadoEntity estado) {
        this.nomeCidade = nomeCidade;
        this.estado = estado;
    }

    public String getNomecidade() {

        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {

        this.nomeCidade = nomeCidade;
    }

    public EstadoEntity getEstado() {

        return estado;
    }

    public void setEstado(EstadoEntity estado) {

        this.estado = estado;
    }

}
