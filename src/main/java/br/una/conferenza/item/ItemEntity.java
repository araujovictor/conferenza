package br.una.conferenza.item;

import br.una.conferenza.localProduto.LocalProdutoEntity;
import br.una.conferenza.notas.NotasEntity;
import br.una.conferenza.produto.ProdutoEntity;
import br.una.conferenza.utils.BaseEntity;
import io.swagger.models.auth.In;

import javax.persistence.*;

@Entity
@Table(name = "tb_item")
@AttributeOverride(name = "id", column = @Column(name= "cod_item"))
public class ItemEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "valor_unitario", nullable = false)
    private Double valorUnitario;

    @Column(name = "status", nullable = false)
    private String statusItem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cod_produto", nullable = false)
    private ProdutoEntity produto;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cod_localproduto", nullable = false)
    private LocalProdutoEntity localProduto;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "numero_nota", nullable = false)
    private NotasEntity nota;

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public String getStatusItem() {
        return statusItem;
    }

    public void setStatusItem(String statusItem) {
        this.statusItem = statusItem;
    }

    public ProdutoEntity getProduto() {
        return produto;
    }

    public void setProduto(ProdutoEntity produto) {
        this.produto = produto;
    }

    public LocalProdutoEntity getLocalProduto() {
        return localProduto;
    }

    public void setLocalProduto(LocalProdutoEntity localProduto) {
        this.localProduto = localProduto;
    }

    public NotasEntity getNota() {
        return nota;
    }

    public void setNota(NotasEntity nota) {
        this.nota = nota;
    }
}
