package br.una.conferenza.permissao;

import br.una.conferenza.utils.BaseEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tb_permissao")
@AttributeOverride(name = "id", column = @Column(name = "cod_permissao"))
public class PermissaoEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column (name = "nome_permissao", nullable = false)
    private String nomePermissao;

    public String getNomePermissao() {
        return nomePermissao;
    }

    public void setNomePermissao(String nomePermissao) {
        this.nomePermissao = nomePermissao;
    }
}
