package br.una.conferenza.produto;

import br.una.conferenza.localProduto.LocalProdutoEntity;
import br.una.conferenza.utils.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "tb_produto")
@AttributeOverride(name = "id", column = @Column(name = "cod_produto"))
public class ProdutoEntity extends BaseEntity <Long> {

    private static final long serialVersionUID = 1L;

    @Column (name = "nome_produto", nullable = false)
    private String nomeProduto;

    @Column(name = "tipo_produto", nullable = false)
    private String tipoProduto;

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(String tipoProduto) {
        this.tipoProduto = tipoProduto;
    }
}
