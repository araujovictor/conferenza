package br.una.conferenza.logradouro;

import javax.persistence.*;

import br.una.conferenza.bairro.BairroEntity;
import br.una.conferenza.utils.BaseEntity;

@Entity
@Table(name = "tb_logradouro")
@AttributeOverride(name = "id", column = @Column(name = "codigo_logradouro"))
public class LogradouroEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome_logradouro", length = 50, nullable = false)
    private String nomeLogradouro;

    @Column(name = "cep_logradouro", length = 8, nullable = false)
    private String cepLogradouro;

    @ManyToOne
    @JoinColumn(name = "codigo_bairro", nullable = false)
    private BairroEntity bairro;

    public LogradouroEntity() {

    }

    public LogradouroEntity(String nomeLogradouro, String cepLogradouro, BairroEntity bairro) {
        this.nomeLogradouro = nomeLogradouro;
        this.cepLogradouro = cepLogradouro;
        this.bairro = bairro;
    }

    public String getNomeLogradouro() {

        return nomeLogradouro;
    }

    public void setNomeLogradouro(String nomeLogradouro) {

        this.nomeLogradouro = nomeLogradouro;
    }

    public String getCepLogradouro() {

        return cepLogradouro;
    }

    public void setCepLogradouro(String cepLogradouro) {

        this.cepLogradouro = cepLogradouro;
    }

    public BairroEntity getBairro() {

        return bairro;
    }

    public void setBairro(BairroEntity bairro) {

        this.bairro = bairro;
    }
}