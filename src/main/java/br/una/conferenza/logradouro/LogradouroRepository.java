package br.una.conferenza.logradouro;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LogradouroRepository extends JpaRepository<LogradouroEntity, Long> {

}
