package br.una.conferenza.notas;

import br.una.conferenza.utils.BaseEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "tb_notas")
@AttributeOverride(name = "id", column = @Column(name= "cod_nota"))
public class NotasEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "numero_nota", nullable = false)
    private String numeroNota;

    @Column(name ="modelo_nota", nullable = false)
    private String modeloNota;

    @Column(name ="serie_nota", nullable = false)
    private String serieNota;

    @Column(name ="dt_emissao", nullable =  false)
    private Date dataEmissao;

    @Column(name = "valor_nota", nullable = false)
    private Double valorNota;

    @Column(name ="xml_nota", nullable = false)
    private byte[] xmlNota;

    @Column(name = "cod_usuinc", nullable = false)
    private Integer codUsuarioInc;

    @Column(name = "cod_fornecedor", nullable = false)
    private Integer codFornecedor;

    public String getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(String numeroNota) {
        this.numeroNota = numeroNota;
    }

    public String getModeloNota() {
        return modeloNota;
    }

    public void setModeloNota(String modeloNota) {
        this.modeloNota = modeloNota;
    }

    public String getSerieNota() {
        return serieNota;
    }

    public void setSerieNota(String serieNota) {
        this.serieNota = serieNota;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Double getValorNota() {
        return valorNota;
    }

    public void setValorNota(Double valorNota) {
        this.valorNota = valorNota;
    }

    public byte[] getXmlNota() {
        return xmlNota;
    }

    public void setXmlNota(byte[] xmlNota) {
        this.xmlNota = xmlNota;
    }

    public Integer getCodUsuarioInc() {
        return codUsuarioInc;
    }

    public void setCodUsuarioInc(Integer codUsuarioInc) {
        this.codUsuarioInc = codUsuarioInc;
    }

    public Integer getCodFornecedor() {
        return codFornecedor;
    }

    public void setCodFornecedor(Integer codFornecedor) {
        this.codFornecedor = codFornecedor;
    }
}
