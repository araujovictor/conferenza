package br.una.conferenza.notas;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NotasRepositoty extends JpaRepository<NotasEntity, Long> {

}