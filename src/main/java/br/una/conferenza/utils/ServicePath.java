package br.una.conferenza.utils;

public final class ServicePath {

    public static final String ALL = "/**";

    public static final String ROOT_PATH = "/api";

    public static final String PUBLIC_ROOT_PATH = ROOT_PATH + "/public";

    public static final String PRIVATE_ROOT_PATH = ROOT_PATH + "/private";

    public static final String LOGIN_PATH = PUBLIC_ROOT_PATH + "/login";

    public static final String LOGOUT_PATH = PUBLIC_ROOT_PATH + "/logout";

    public static final String USER_PATH =  PUBLIC_ROOT_PATH + "/user";

    public static final String PERMISSION_PATH = PUBLIC_ROOT_PATH + "/permission";

    private ServicePath() {}
}
