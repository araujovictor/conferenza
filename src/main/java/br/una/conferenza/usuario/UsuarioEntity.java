package br.una.conferenza.usuario;

import br.una.conferenza.permissao.PermissaoEntity;
import br.una.conferenza.utils.BaseEntity;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "tb_usuario")
@AttributeOverride(name = "id", column = @Column(name = "cod_usuario"))
public class UsuarioEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome_usuario", nullable = false)
    private String nomeUsuario;

    @Column(name = "email_usuario", nullable = false, unique = true)
    private String emailUsuario;

    @Size(min = 4, max = 20)
    @Column(name = "senha_usuario",length = 20, nullable = false)
    private String senhaUsuario;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tb_usuario_permissao", joinColumns = @JoinColumn(name = "usuario"), inverseJoinColumns = @JoinColumn(name = "permissao"))
    private List<PermissaoEntity> permissoes;

    public UsuarioEntity(){

    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario(String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public List<PermissaoEntity> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<PermissaoEntity> permissoes) {
        this.permissoes = permissoes;
    }
}
