package br.una.conferenza.usuario;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long> {

    UsuarioEntity findByEmailUsuario(String email);

    public List<UsuarioEntity> findByNomeUsuario(String nome);
}
