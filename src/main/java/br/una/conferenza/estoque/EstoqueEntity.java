package br.una.conferenza.estoque;

import br.una.conferenza.utils.BaseEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tb_estoque")
@AttributeOverride(name = "id", column = @Column(name = "cod_estoque"))
public class EstoqueEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column(name = "qnt_maxima", nullable = false)
    private Integer qntMaxima;

    @Column(name = "qnt_minima", nullable = false)
    private Integer qntMinima;

    @Column(name = "qnt_atual", nullable = false)
    private Integer qntAtual;

    @Column(name = "cod_produto",nullable = false)
    private Integer codProduto;

    public Integer getQntMaxima() {
        return qntMaxima;
    }

    public void setQntMaxima(Integer qntMaxima) {
        this.qntMaxima = qntMaxima;
    }

    public Integer getQntMinima() {
        return qntMinima;
    }

    public void setQntMinima(Integer qntMinima) {
        this.qntMinima = qntMinima;
    }

    public Integer getQntAtual() {
        return qntAtual;
    }

    public void setQntAtual(Integer qntAtual) {
        this.qntAtual = qntAtual;
    }

    public Integer getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(Integer codProduto) {
        this.codProduto = codProduto;
    }
}
