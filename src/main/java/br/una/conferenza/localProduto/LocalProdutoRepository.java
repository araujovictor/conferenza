package br.una.conferenza.localProduto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalProdutoRepository extends JpaRepository<LocalProdutoEntity, Long> {

}
