package br.una.conferenza.localProduto;

import br.una.conferenza.utils.BaseEntity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tb_localproduto")
@AttributeOverride(name = "id", column = @Column(name = "cod_localproduto"))
public class LocalProdutoEntity extends BaseEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Column (name = "tb_descricao_localproduto", nullable = false)
    private String descricaoLocal;

    public String getDescricaoLocal() {
        return descricaoLocal;
    }

    public void setDescricaoLocal(String descricaoLocal) {
        this.descricaoLocal = descricaoLocal;
    }
}
