package br.una.conferenza.bairro;

import javax.persistence.*;

import br.una.conferenza.cidade.CidadeEntity;
import br.una.conferenza.utils.BaseEntity;

@Entity
@Table(name = "tb_bairros")
@AttributeOverride(name = "id", column = @Column(name = "codigo_bairro"))
public class BairroEntity extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column(name = "nome_bairro", length = 45, nullable = false)
	private String nomeBairro;

	@ManyToOne
	@JoinColumn(name = "codigo_cidade", nullable = false)
	private CidadeEntity cidade;

	public BairroEntity() {

	}

	public BairroEntity(String nomeBairro, CidadeEntity cidade) {
		this.nomeBairro = nomeBairro;
		this.cidade = cidade;
	}

	public String getNomeBairro() {
		return nomeBairro;
	}

	public void setNomeBairro(String nomeBairro) {
		this.nomeBairro = nomeBairro;
	}

	public CidadeEntity getCidade() {
		return cidade;
	}

	public void setNomeCidade(CidadeEntity cidade) {
		this.cidade = cidade;
	}

}