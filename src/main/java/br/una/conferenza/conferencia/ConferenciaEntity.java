package br.una.conferenza.conferencia;

import javax.persistence.*;

import br.una.conferenza.usuario.UsuarioEntity;
import br.una.conferenza.utils.BaseEntity;

@Entity
@Table(name = "tb_conferencia")
@AttributeOverride(name = "id", column = @Column(name= "cod_conferecia"))
public class ConferenciaEntity extends BaseEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "numero_nota", length = 30, nullable = false)
	private String numeroNota;

	@Column(name= "quantidade", nullable = false)
    private Double quantidade;

    @Column(name = "quantidade_contada", nullable = false)
    private Double quantidadeContada;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cod_usuario")
    private UsuarioEntity usuarioEntity;

    public String getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(String numeroNota) {
        this.numeroNota = numeroNota;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getQuantidadeContada() {
        return quantidadeContada;
    }

    public void setQuantidadeContada(Double quantidadeContada) {
        this.quantidadeContada = quantidadeContada;
    }

    public UsuarioEntity getUsuarioEntity() {
        return usuarioEntity;
    }

    public void setUsuarioEntity(UsuarioEntity usuarioEntity) {
        this.usuarioEntity = usuarioEntity;
    }
}
