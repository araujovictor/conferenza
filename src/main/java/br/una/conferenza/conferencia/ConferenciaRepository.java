package br.una.conferenza.conferencia;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConferenciaRepository extends JpaRepository<ConferenciaEntity, Long> {

}
