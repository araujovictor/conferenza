package br.una.conferenza.usuariopermissao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioPermissaoRepository extends JpaRepository<UsuarioPermissaoEntity, Long> {

    public UsuarioPermissaoEntity findById(UsuarioPermissaoKey usuarioPermissaoKey);
}
