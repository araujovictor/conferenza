package br.una.conferenza.usuariopermissao;

import br.una.conferenza.utils.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tb_usuario_permissao")
public class UsuarioPermissaoEntity extends BaseEntity<UsuarioPermissaoKey> {

}
