package br.una.conferenza.usuariopermissao;

import br.una.conferenza.utils.BaseKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UsuarioPermissaoKey extends BaseKey {

    private static final long serialVersionUID = 1L;

    @Column(name = "cod_permissao", nullable = false)
    private Long permissao;

    @Column(name = "cod_usuario", nullable = false)
    private Long usuario;

    public Long getPermissao() {
        return permissao;
    }

    public void setPermissao(Long permissao) {
        this.permissao = permissao;
    }

    public Long getUsuario() {
        return usuario;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((this.permissao == null) ? 0 : this.permissao.hashCode());
        result = prime * result + ((this.usuario == null) ? 0 : this.usuario.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj){
            return true;
        }

        if (!super.equals(obj)){
            return false;
        }

        if (this.getClass() != obj.getClass()){
            return false;
        }

        UsuarioPermissaoKey other = (UsuarioPermissaoKey) obj;

        if (this.permissao == null){
            if (other.permissao != null){
                return false;
            }
        } else if (!this.permissao.equals(other.permissao)){
            return false;
        }

        if (this.usuario == null){
            if (other.usuario != null){
                return false;
            }
        } else if (!this.usuario.equals(other.usuario)){
            return false;
        }

        return true;
    }

}
