package br.una.conferenza.estado;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoRepository extends JpaRepository<EstadoEntity, Long> {

	public EstadoEntity findByNomeEstado(String nomeEstado);

}